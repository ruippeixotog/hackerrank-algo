WARMUP_CC_FILES = $(wildcard warmup/*.cpp)
WARMUP_BIN_FILES = $(patsubst warmup/%.cpp, %, $(WARMUP_CC_FILES))
SORTING_CC_FILES = $(wildcard arrays-and-sorting/*.cpp)
SORTING_BIN_FILES = $(patsubst arrays-and-sorting/%.cpp, %, $(SORTING_CC_FILES))

BIN_FILES = $(WARMUP_BIN_FILES) $(SORTING_BIN_FILES)
CC_FLAGS = -Wall -O2 -lm
CC = g++

all: $(BIN_FILES)

$(WARMUP_BIN_FILES):%: warmup/%.cpp
	$(CC) $(CC_FLAGS) -o $@ $<

$(SORTING_BIN_FILES):%: arrays-and-sorting/%.cpp
	$(CC) $(CC_FLAGS) -o $@ $<

clean:
	rm -f $(BIN_FILES)
