# Solutions to HackerRank Algorithmic Challenges

This repository contains solutions to the HackerRank Algorithmic Challenges, available at [https://www.hackerrank.com/categories/algorithms][1]. These solutions are provided "as is". I give no guarantees that they will work as expected. Please refrain from using my solutions in the site.

## Problems solved

Here is a list of the problems currently in this repository. Problems marked with ✓ are done, while problems with ✗ are not complete and/or don't comply with performance requirements.

### Warmup

* ✓ Solve me first (`solve-me-first.cpp`)
* ✓ Utopian Tree (`utopian-tree.cpp`)
* ✓ Service Lane (`service-lane.cpp`)
* ✓ The Love-Letter Mystery (`the-love-letter-mystery.cpp`)
* ✓ Gem Stones (`gem-stones.cpp`)
* ✓ Halloween party (`halloween-party.cpp`)
* ✓ Game of Thrones - I (`game-of-thrones.cpp`)
* ✓ Maximizing XOR (`maximizing-xor.cpp`)
* ✓ Chocolate Feast (`chocolate-feast.cpp`)
* ✓ Manasa and Stones (`manasa-and-stones.cpp`)
* ✓ Find Digits (`find-digits.cpp`)
* ✓ Filling Jars (`filling-jars.cpp`)
* ✓ Sherlock and The Beast (`sherlock-and-the-beast.cpp`)
* ✓ Sherlock and GCD (`sherlock-and-gcd.cpp`)
* ✓ Angry Children (`angry-children.cpp`)
* ✓ Is Fibo (`is-fibo.cpp`)
* ✓ Sherlock and Queries (`sherlock-and-queries.cpp`)

### Arrays and Sorting

* ✓ Intro to Tutorial Challenges (`tutorial-intro.cpp`)
* ✓ Insertion Sort - Part 1 (`InsertionSort1.java`)
* ✓ Insertion Sort - Part 2 (`InsertionSort2.java`)
* ✓ Correctness and the Loop Invariant (`correctness-invariant.cpp`)
* ✓ Running Time of Algorithms (`RunningTime.java`)
* ✓ QuickSort1 - Partition (`QuickSort1.java`)
* ✓ QuickSort (`QuickSort2.java`)
* ✓ Running Time of Quicksort (`quicksort4.cpp`)
* ✓ Counting Sort 1 (`countingsort1.cpp`)
* ✓ Counting Sort 2 (`countingsort2.cpp`)
* ✓ Counting Sort 3 (`countingsort3.cpp`)
* ✗ Insertion Sort Advanced Analysis (`insertion-sort.cpp`)

### Search

* ✓ Pairs (`Pairs.java`)
* ✓ Arithmetic Progressions (`ArithmeticProgressions.java`)
* ✓ Median (`Median.java`)
* ✓ Flowers (`Flowers.java`)

### Bit Manipulation

* ✗ XOR Key (`XorKey.java`)

[1]: https://www.hackerrank.com/categories/algorithms
