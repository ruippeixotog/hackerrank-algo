#include <algorithm>
#include <iostream>

#define ll long long

using namespace std;

ll fibs[50];

int genFib() {
  ll a = 0, b = 1;
  int i = 0;
  while(b < 1e10) {
    fibs[i] = a + b; a = b; b = fibs[i++];
  }
  return i;
}

int main() {
  int nFibs = genFib();

  int t; cin >> t;
  for(int tc = 1; tc <= t; tc++) {
    ll n; cin >> n;

    cout << (binary_search(fibs, fibs + nFibs, n) ?
      "IsFibo" : "IsNotFibo") << endl;
  }
  return 0;
}
