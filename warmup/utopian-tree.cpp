#include <iostream>

using namespace std;

int main() {
  int t; cin >> t;
  for(int tc = 1; tc <= t; tc++) {
    int n; cin >> n;

    int h = 1;
    for(int i = 0; i < n; i++) {
      if(i % 2) h++;
      else h *= 2;
    }
    cout << h << endl;
  }
  return 0;
}
