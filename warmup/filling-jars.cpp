#include <iostream>

#define ll long long

using namespace std;

int main() {
  int n, m; cin >> n >> m;
  ll count = 0;

  for(int i = 0; i < m; i++) {
    ll a, b, k; cin >> a >> b >> k;
    count += (b - a + 1) * k;
  }
  cout << count / n << endl;
  return 0;
}
