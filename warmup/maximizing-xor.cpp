#include <cmath>
#include <iostream>

using namespace std;

int main() {
  int l, r; cin >> l >> r;
  int x = 0;
  for(int a = l; a <= r; a++) {
    for(int b = a; b <= r; b++) {
      x = max(x, a ^ b);
    }
  }
  cout << x << endl;
  return 0;
}
