#include <iostream>

using namespace std;

int main() {
  int t; cin >> t;
  for(int tc = 1; tc <= t; tc++) {
    int n, c, m; cin >> n >> c >> m;

    int total = n / c;
    int wraps = total;
    while(wraps >= m) {
      int freec = wraps / m;
      total += freec;
      wraps = wraps - freec * m + freec;
    }
    cout << total << endl;
  }
  return 0;
}
