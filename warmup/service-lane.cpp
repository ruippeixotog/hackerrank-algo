#include <cmath>
#include <iostream>

#define MAXN 100000

using namespace std;

int width[MAXN];

int main() {
  int n, t; cin >> n >> t;
  for(int i = 0; i < n; i++) cin >> width[i];

  for(int tc = 1; tc <= t; tc++) {
    int i0, i1; cin >> i0 >> i1; 

    int res = 3;
    for(int i = i0; i <= i1; i++)
      res = min(res, width[i]);

    cout << res << endl;
  }
  return 0;
}
