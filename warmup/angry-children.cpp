#include <algorithm>
#include <iostream>

#define MAXN 100000
#define INF 2e9

using namespace std;

int candies[MAXN];

int main() {
  int n, k; cin >> n >> k;
  for(int i = 0; i < n; i++) cin >> candies[i];

  sort(candies, candies + n);

  int res = INF;
  for(int i = 0; i < n - k; i++) {
    res = min(res, candies[i + k - 1] - candies[i]);
  }
  cout << res << endl;
  return 0;
}
