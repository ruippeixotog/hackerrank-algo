#include <iostream>

using namespace std;

void swap(int& a, int& b) {
  int temp = a;
  a = b;
  b = temp;
}

int main() {
  int t; cin >> t;
  for(int tc = 1; tc <= t; tc++) {
    int n, a, b; cin >> n >> a >> b;
    n--;

    if(a == b) cout << a * n << endl;
    else {
      if(a > b) swap(a, b);

      cout << a * n;
      for(int i = 1; i <= n; i++) {
        cout << " " << (a * (n - i) + b * i);  
      }
      cout << endl;
    }
  }
  return 0;
}
