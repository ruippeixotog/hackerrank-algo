#include <iostream>

#define ll long long

using namespace std;

int main() {
  int t; cin >> t;
  for(int tc = 1; tc <= t; tc++) {
    int k; cin >> k;
    ll half = k / 2;
    cout << half * (k % 2 ? half + 1 : half) << endl;
  }
  return 0;
}
