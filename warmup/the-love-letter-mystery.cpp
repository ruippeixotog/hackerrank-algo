#include <cmath>
#include <iostream>
#include <string>

using namespace std;

int main() {
  int t; cin >> t;
  for(int tc = 1; tc <= t; tc++) {
    string word; cin >> word;

    int ops = 0;
    for(int i = 0; i < word.length() / 2; i++)
      ops += abs(word[i] - word[word.length() - i - 1]);

    cout << ops << endl;
  }
  return 0;
}
