#include <iostream>

using namespace std;

int main() {
  int t; cin >> t;
  for(int tc = 1; tc <= t; tc++) {
    int n; cin >> n;

    int digits = n;
    int count = 0;

    while(digits > 0) {
      int digit = digits % 10;
      if(digit != 0 && n % digit == 0) count++;
      digits /= 10;
    }

    cout << count << endl;
  }
  return 0;
}
