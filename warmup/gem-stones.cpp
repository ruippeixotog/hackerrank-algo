#include <algorithm>
#include <iostream>

using namespace std;

int counter[26];

string findGems(string str) {
  sort(str.begin(), str.end());
  string::iterator it = unique(str.begin(), str.end());
  str.resize(distance(str.begin(), it));
  return str;
}

int main() {
  int n; cin >> n;

  for(int i = 0; i < n; i++) {
    string str; cin >> str;
    string gems = findGems(str);

    for(int j = 0; j < gems.length(); j++)
      counter[gems[j] - 'a']++;
  }

  int nGems = 0;
  for(int i = 0; i < 26; i++) {
    if(counter[i] == n) nGems++;
  }
  cout << nGems << endl;

  return 0;
}
