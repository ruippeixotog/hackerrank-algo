#include <iostream>
#include <string>

using namespace std;

int counter[26];

int main() {
  string str; cin >> str;
  for(int i = 0; i < str.length(); i++)
    counter[str[i] - 'a']++;

  bool hasOdd = false;
  string res = "YES";
  for(int i = 0; i < 26; i++) {
    if(counter[i] % 2 != 0) {
      if(hasOdd) { res = "NO"; break; }
      else hasOdd = true; 
    }
  }
  cout << res << endl;
  return 0;
}
