#include <iostream>

using namespace std;

int main() {
  int t; cin >> t;
  for(int tc = 1; tc <= t; tc++) {
    int n; cin >> n;

    int fives = n;
    while(fives >= 0 && (fives % 3 != 0 || (n - fives) % 5 != 0))
      fives--;

    if(fives == -1) cout << -1 << endl;
    else {
      for(int i = 0; i < n; i++) {
        cout << (i < fives ? 5 : 3);
      }
      cout << endl;
    }
  }
  return 0;
}
