#include <iostream>

#define MAXX 100

using namespace std;

int cnt[MAXX];

int main() {
  int n; cin >> n;
  for(int i = 0; i < n; i++) {
    int x; string str; cin >> x >> str;
    cnt[x]++;
  }

  for(int i = 1; i < MAXX; i++)
    cnt[i] += cnt[i - 1];

  cout << cnt[0];
  for(int i = 1; i < MAXX; i++)
    cout << " " << cnt[i];
  cout << endl;

  return 0;
}
