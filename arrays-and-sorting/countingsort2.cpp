#include <iostream>

#define MAXX 100

using namespace std;

int cnt[MAXX];

int main() {
  int n; cin >> n;
  for(int i = 0; i < n; i++) {
    int x; cin >> x;
    cnt[x]++;
  }

  for(int i = 0; i < MAXX; i++) {
    for(int j = 0; j < cnt[i]; j++) {
      cout << i << " ";
    }
  }

  return 0;
}
