#include <algorithm>
#include <iostream>

#define MAXN 1000

using namespace std;

int arr[MAXN];

int main() {
  int v, n; cin >> v >> n;
  for(int i = 0; i < n; i++) cin >> arr[i];

  cout << (lower_bound(arr, arr + n, v) - arr) << endl;
  return 0;
}
