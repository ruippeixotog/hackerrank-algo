#include <iostream>
#include <utility>

using namespace std;

struct Tree {
  pair<int, int> val;
  int size;
  Tree *left, *right;

  Tree(): size(0), left(NULL), right(NULL) {}
  Tree(pair<int, int> v): val(v), size(1), left(NULL), right(NULL) {}

  void add(pair<int, int> v) {
    if(size == 0) {
      val = v;
    } else if(v < val) {
      if(left) left->add(v);
      else left = new Tree(v);
    } else {
      if(right) right->add(v);
      else right = new Tree(v);
    }
    size++;
  }

  int indexOf(pair<int, int> v) {
    if(size == 0) return 0;
    if(v < val) {
      return left ? left->indexOf(v) : 0;
    } else {
      int lSize = left ? left->size + 1 : 1;
      return lSize + (right ? right->indexOf(v) : 0);
    }
  }

  ~Tree() {
    if(left) delete left;
    if(right) delete right;
  }
};

int main() {
  int t; cin >> t;
  for(int tc = 1; tc <= t; tc++) {
    int n; cin >> n;

    Tree t;
    int cnt = 0;

    for(int i = 0; i < n; i++) {
      int x; cin >> x;
      int dist = t.indexOf(make_pair(x, i));
      cnt += (i - dist);
      t.add(make_pair(x, i));
    }
    cout << cnt << endl;
  }
  return 0;
}
