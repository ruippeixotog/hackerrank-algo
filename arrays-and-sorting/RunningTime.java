import java.io.PrintStream;
import java.util.*;

public class RunningTime {
	
	static Scanner in;
	static PrintStream out;

	public static void insertionSort(int[] ar) {
		int count = 0;
		for (int i = 1; i < ar.length; i++) {
			for (int j = i - 1; j >= 0 && ar[j] > ar[j + 1]; j--) {
				swap(ar, j, j + 1);
				count++;
			}
		}
		out.println(count);
	}

	static void swap(int[] ar, int i, int j) {
		int tmp = ar[i];
		ar[i] = ar[j];
		ar[j] = tmp;
	}

	public static void main(String[] args) {
		in = new Scanner(System.in);
		out = System.out;
		
		int n = in.nextInt();
		int[] ar = new int[n];
		for (int i = 0; i < n; i++) {
			ar[i] = in.nextInt();
		}
		insertionSort(ar);
	}
}