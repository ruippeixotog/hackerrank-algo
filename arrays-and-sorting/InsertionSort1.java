/* Head ends here */
import java.util.*;

public class InsertionSort1 {

	static void insertionSort(int[] ar) {
		int last = ar[ar.length - 1];
		for(int i = ar.length - 1; i > 0; i--) {
			if(last < ar[i - 1]) {
				ar[i] = ar[i - 1];
				printArray(ar);
			} else {
				ar[i] = last;
				printArray(ar);
				return;
			}
		}
		ar[0] = last;
		printArray(ar);
	}

	/* Tail starts here */
	
	static void printArray(int[] ar) {
		for (int n : ar) {
			System.out.print(n + " ");
		}
		System.out.println("");
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] ar = new int[n];
		for (int i = 0; i < n; i++) {
			ar[i] = in.nextInt();
		}
		insertionSort(ar);
	}
}